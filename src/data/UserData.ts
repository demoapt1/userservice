import {getRepository} from "typeorm";
import {User} from "../entity/User";

export class UserData {

    private userRepository = getRepository(User);

    async authenticate(params?: any) {
        return this.userRepository.findOne({ email: params.email, password: params.password });
    }

    async all(params?: any) {
        if (!params) {
            return this.userRepository.find();
        } else {
            return this.userRepository.find(params);
        }
    }

    async one(params?: any) {
        return this.userRepository.findOne(params);
    }

    async save(params?: any) {
        let users = await this.userRepository.findOne({ UserID: params.UserID });
        if (!users) {
            users = await this.userRepository.create({
                        UserID: params.UserID,
                        email: params.email,
                        password: params.password,
                        firstName: params.firstName,
                        lastName: params.lastName,
                        DoB: params.DoB
                    });
        } else {
            users.email = params.email;
            users.password = params.password;
            users.firstName = params.firstName;
            users.lastName = params.lastName;
            users.DoB = params.DoB;
        }
        return this.userRepository.save(users);
    }

    async remove(params?: any) {
        let userToRemove = await this.userRepository.findOne(params);
        await this.userRepository.remove(userToRemove);
    }

}